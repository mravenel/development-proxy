#!/usr/bin/env bash

mkdir -p security/certs

echo "Username:"
read -r username

htpasswd -Bc "security/.htpasswd" "${username}"

echo "Installing certificate authority certificate"
mkcert -install

echo "Generating certificates for *.app.localhost"
mkcert \
    -cert-file "security/certs/_wildcard.app.localhost.pem" \
    -key-file "security/certs/_wildcard.app.localhost-key.pem" \
    "*.app.localhost"

echo "Generating certificates for *.dev.localhost"
mkcert \
    -cert-file "security/certs/_wildcard.dev.localhost.pem" \
    -key-file "security/certs/_wildcard.dev.localhost-key.pem" \
    "*.dev.localhost"
