# Traefik Development Proxy

A proxy for working with dockerized applications in development.

## Requirements

* [docker]()
* [docker-compose]()
* htpasswd
* [mkcert](https://github.com/FiloSottile/mkcert)

### Mac OS
* dnsmasq

## Installation
Use the security.sh script to generate a user and ssl certificates.

```bash
./security.sh
```

## Usage

Start the proxy and the sample app with:

```bash
docker-compose up -d
```

The Traefik dashboard is at the URL https://traefik.app.localhost, and the sample
app is at the URL https://whoami.app.localhost.

